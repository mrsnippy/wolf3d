/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:29:31 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:21:20 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "libft.h"
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <time.h>

# define WIN_W 1000
# define WIN_H 1000
# define PL pool->player
# define RAY pool->ray
# define MAP pool->map
# define DDA pool->dda
# define MLX pool->mlx
# define FL pool->flags
# define COLOR pool->color

typedef struct	s_mlx
{
	void	*init;
	void	*win;
	void	*img;
}				t_mlx;

typedef struct	s_player
{
	double	xpos;
	double	ypos;
	double	drx;
	double	dry;
	double	plx;
	double	ply;
	double	time;
	double	otime;
	double	speed;
	double	rspeed;
}				t_player;

typedef struct	s_ray
{
	double camx;
	double rpx;
	double rpy;
	double rdx;
	double rdy;
}				t_ray;

typedef	struct	s_dda
{
	int		mapx;
	int		mapy;
	double	sdx;
	double	sdy;
	double	ddx;
	double	ddy;
	double	dist;
	int		xstep;
	int		ystep;
	int		bingo;
	int		side;
}				t_dda;

typedef	struct	s_map
{
	int **map;
	int deep;
	int size;
	int lh;
	int dstart;
	int dstop;
}				t_map;

typedef struct	s_flags
{
	int mfflag;
	int mbflag;
	int rlflag;
	int rrflag;
}				t_flags;

typedef struct	s_pool
{
	t_mlx		*mlx;
	t_player	*player;
	t_ray		*ray;
	t_dda		*dda;
	t_map		*map;
	t_flags		*flags;
	int			color;
}				t_pool;

int				ft_exit(void *a);
void			ft_read(int ***imap, int *d, int *s);
void			ft_elton(int *x, int *y, int color, t_mlx *mlx);
void			ft_raycast(t_pool *pool);
void			ft_redraw(t_pool *pool);

void			ft_rotation(t_pool *pool, int flag);
void			ft_move_forward(t_pool *pool);
void			ft_move_backward(t_pool *pool);
int				ft_kpress(int keycode, t_pool *pool);
int				ft_krel(int keycode, t_pool *pool);

void			ft_player_init(t_pool *pool);
void			ft_ray_dda_init(t_pool *pool);
void			ft_dda_steps(t_pool *pool);
void			ft_dda_cast(t_pool *pool);
void			ft_dda_calc(t_pool *pool);

#endif
