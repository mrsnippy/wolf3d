/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 17:59:07 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:15:24 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_player_init(t_pool *pool)
{
	PL->xpos = 5;
	PL->ypos = 2;
	PL->drx = -1;
	PL->dry = 0;
	PL->plx = 0;
	PL->ply = 0.66;
	PL->time = 0;
	PL->otime = 0;
	FL->mfflag = 0;
	FL->mbflag = 0;
	FL->rlflag = 0;
	FL->rrflag = 0;
}

void	ft_ray_dda_init(t_pool *pool)
{
	RAY->rpx = PL->xpos;
	RAY->rpy = PL->ypos;
	RAY->rdx = PL->drx + PL->plx * RAY->camx;
	RAY->rdy = PL->dry + PL->ply * RAY->camx;
	DDA->mapx = (int)RAY->rpx;
	DDA->mapy = (int)RAY->rpy;
	DDA->ddx = sqrt(1 + (RAY->rdy * RAY->rdy)
		/ (RAY->rdx * RAY->rdx));
	DDA->ddy = sqrt(1 + (RAY->rdx * RAY->rdx)
		/ (RAY->rdy * RAY->rdy));
}

void	ft_dda_steps(t_pool *pool)
{
	if (RAY->rdx < 0)
	{
		DDA->xstep = -1;
		DDA->sdx = (RAY->rpx - DDA->mapx) * DDA->ddx;
	}
	else
	{
		DDA->xstep = 1;
		DDA->sdx = (DDA->mapx + 1.0 - RAY->rpx) * DDA->ddx;
	}
	if (RAY->rdy < 0)
	{
		DDA->ystep = -1;
		DDA->sdy = (RAY->rpy - DDA->mapy) * DDA->ddy;
	}
	else
	{
		DDA->ystep = 1;
		DDA->sdy = (DDA->mapy + 1.0 - RAY->rpy) * DDA->ddy;
	}
}

void	ft_dda_cast(t_pool *pool)
{
	while (DDA->bingo == 0)
	{
		if (DDA->sdx < DDA->sdy)
		{
			DDA->sdx += DDA->ddx;
			DDA->mapx += DDA->xstep;
			DDA->side = 0;
		}
		else
		{
			DDA->sdy += DDA->ddy;
			DDA->mapy += DDA->ystep;
			DDA->side = 1;
		}
		if ((MAP->map)[DDA->mapx][DDA->mapy] > 0)
			DDA->bingo = 1;
	}
}

void	ft_dda_calc(t_pool *pool)
{
	if (DDA->side == 0)
		DDA->dist = (DDA->mapx - RAY->rpx +
			(1 - DDA->xstep) / 2) / RAY->rdx;
	else
		DDA->dist = (DDA->mapy - RAY->rpy +
			(1 - DDA->ystep) / 2) / RAY->rdy;
	MAP->lh = (int)(WIN_H / DDA->dist);
	MAP->dstart = -(MAP->lh) / 2 + WIN_H / 2;
	if (MAP->dstart < 0)
		MAP->dstart = 0;
	MAP->dstop = MAP->lh / 2 + WIN_H / 2;
	if (MAP->dstop >= WIN_H)
		MAP->dstop = WIN_H - 1;
}
