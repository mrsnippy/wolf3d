/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   elton.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/27 12:51:04 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 15:28:34 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void			ft_i(int x, int y, int rgb, t_mlx *mlx)
{
	int				bpp;
	int				sl;
	int				en;
	char			*image;
	unsigned int	tmp;

	image = mlx_get_data_addr(mlx->img, &bpp, &sl, &en);
	tmp = (mlx_get_color_value(mlx->init, rgb));
	if (x > 0 && x < WIN_W && y > 0 && y < WIN_H)
	{
		ft_memcpy((void *)(image + sl * y + x * sizeof(int)),
			(void *)&tmp, 4);
	}
}

static void		ft_john(float **ds, float **err, int **x, int **y)
{
	(*ds)[0] = abs((*x)[1] - (*x)[0]);
	(*ds)[1] = abs((*y)[1] - (*y)[0]);
	(*ds)[2] = ((*x)[0] < (*x)[1]) ? 1 : -1;
	(*ds)[3] = ((*y)[0] < (*y)[1]) ? 1 : -1;
	(*err)[0] = (*ds)[0] - (*ds)[1];
}

void			ft_elton(int *x, int *y, int color, t_mlx *mlx)
{
	float	*ds;
	float	*err;

	err = (float *)malloc(sizeof(float) * 2);
	ds = (float *)malloc(sizeof(float) * 4);
	ft_john(&ds, &err, &x, &y);
	while (x[0] != x[1] || y[0] != y[1])
	{
		err[1] = err[0] * 2;
		if (err[1] > -ds[1])
		{
			err[0] -= ds[1];
			x[0] += ds[2];
		}
		if (err[1] < ds[0])
		{
			err[0] += ds[0];
			y[0] += ds[3];
		}
		ft_i(x[0], y[0], color, mlx);
	}
	free(err);
	free(ds);
}
