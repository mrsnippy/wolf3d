/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/29 15:34:19 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:13:59 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_exit(void *a)
{
	if (!a)
		exit(0);
	return (0);
}

int		ft_time(t_pool *pool)
{
	double frame_time;

	PL->otime = PL->time;
	PL->time = clock();
	frame_time = (PL->time - PL->otime) / CLOCKS_PER_SEC;
	PL->speed = frame_time * 3.0;
	PL->rspeed = frame_time * 2.0;
	return (0);
}

int		ft_events(t_pool *pool)
{
	ft_time(pool);
	if (FL->rlflag == 1)
		ft_rotation(pool, 0);
	if (FL->rrflag == 1)
		ft_rotation(pool, 1);
	if (FL->mfflag == 1)
		ft_move_forward(pool);
	if (FL->mbflag == 1)
		ft_move_backward(pool);
	mlx_hook(pool->mlx->win, 2, 1, ft_kpress, pool);
	mlx_hook(pool->mlx->win, 3, 2, ft_krel, pool);
	mlx_hook(pool->mlx->win, 17, 0L, ft_exit, NULL);
	return (0);
}

void	ft_picasso(t_pool *pool)
{
	t_ray		*ray;
	t_mlx		*mlx;
	t_dda		*dda;
	t_map		*map;

	mlx = (t_mlx *)malloc(sizeof(t_mlx));
	ray = (t_ray *)malloc(sizeof(t_ray));
	dda = (t_dda *)malloc(sizeof(t_dda));
	map = (t_map *)malloc(sizeof(t_map));
	MLX = mlx;
	RAY = ray;
	DDA = dda;
	MAP = map;
	ft_player_init(pool);
	MLX->init = mlx_init();
	MLX->win = mlx_new_window(MLX->init, WIN_W, WIN_H, "wolf3d");
	MLX->img = mlx_new_image(MLX->init, WIN_W, WIN_H);
	ft_read(&(MAP->map), &(MAP->deep), &(MAP->size));
	ft_raycast(pool);
	mlx_put_image_to_window(MLX->init, MLX->win, MLX->img, 0, 0);
	mlx_loop_hook(MLX->init, ft_events, pool);
	mlx_loop(MLX->init);
}

int		main(void)
{
	t_pool		*pool;
	t_player	*player;
	t_flags		*flags;

	pool = (t_pool *)malloc(sizeof(t_pool));
	player = (t_player *)malloc(sizeof(t_player));
	flags = (t_flags *)malloc(sizeof(t_flags));
	PL = player;
	FL = flags;
	ft_picasso(pool);
	return (0);
}
