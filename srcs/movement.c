/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   movement.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/01 15:10:29 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:26:20 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_rotation(t_pool *pool, int flag)
{
	double o_dir_x;
	double o_plane_x;
	double r_speed;

	r_speed = (flag == 0) ? PL->rspeed : -PL->rspeed;
	o_dir_x = PL->drx;
	PL->drx = PL->drx * cos(r_speed) - PL->dry * sin(r_speed);
	PL->dry = o_dir_x * sin(r_speed) + PL->dry * cos(r_speed);
	o_plane_x = PL->plx;
	PL->plx = PL->plx * cos(r_speed) - PL->ply * sin(r_speed);
	PL->ply = o_plane_x * sin(r_speed) + PL->ply * cos(r_speed);
	ft_redraw(pool);
}

void	ft_move_forward(t_pool *pool)
{
	if (!((MAP->map)[(int)(PL->xpos + PL->drx * PL->speed
		+ 0.3 * PL->drx)][(int)PL->ypos]))
		PL->xpos += PL->drx * PL->speed;
	if (!((MAP->map)[(int)PL->xpos][(int)(PL->ypos + PL->dry * PL->speed
		+ 0.3 * PL->dry)]))
		PL->ypos += PL->dry * PL->speed;
	ft_redraw(pool);
}

void	ft_move_backward(t_pool *pool)
{
	if (!((MAP->map)[(int)(PL->xpos - PL->drx * PL->speed
		- 0.3 * PL->drx)][(int)PL->ypos]))
		PL->xpos -= PL->drx * PL->speed;
	if (!((MAP->map)[(int)PL->xpos][(int)(PL->ypos - PL->dry * PL->speed
		- 0.3 * PL->dry)]))
		PL->ypos -= PL->dry * PL->speed;
	ft_redraw(pool);
}

int		ft_kpress(int keycode, t_pool *pool)
{
	if (keycode == 53)
		ft_exit(NULL);
	if (keycode == 125)
		FL->mbflag = 1;
	if (keycode == 126)
		FL->mfflag = 1;
	if (keycode == 123)
		FL->rlflag = 1;
	if (keycode == 124)
		FL->rrflag = 1;
	return (0);
}

int		ft_krel(int keycode, t_pool *pool)
{
	if (keycode == 125)
		FL->mbflag = 0;
	if (keycode == 126)
		FL->mfflag = 0;
	if (keycode == 123)
		FL->rlflag = 0;
	if (keycode == 124)
		FL->rrflag = 0;
	return (0);
}
