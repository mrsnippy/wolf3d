/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ololo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 18:13:53 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:30:10 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	ft_color_picker(t_pool *pool)
{
	if (DDA->side)
		COLOR = (DDA->ystep < 0) ? 12444215 : 12777215;
	else
		COLOR = (DDA->xstep < 0) ? 0xfa9f1b : 0x808080;
}

void		ft_redraw(t_pool *pool)
{
	mlx_destroy_image(MLX->init, MLX->img);
	MLX->img = mlx_new_image(MLX->init, WIN_W, WIN_H);
	ft_raycast(pool);
	mlx_put_image_to_window(MLX->init, MLX->win, MLX->img, 0, 0);
}

void		ft_foo2(int x, int y1, int y2, t_pool *pool)
{
	int *xx;
	int *yy;

	xx = (int *)malloc(sizeof(int) * 2);
	yy = (int *)malloc(sizeof(int) * 2);
	xx[0] = x;
	xx[1] = x;
	yy[0] = y1;
	yy[1] = y2;
	ft_elton(xx, yy, pool->color, pool->mlx);
	free(xx);
	free(yy);
}

void		ft_raycast(t_pool *pool)
{
	int x;

	x = -1;
	while (++x < WIN_W)
	{
		RAY->camx = 2 * x / (double)WIN_W - 1;
		ft_ray_dda_init(pool);
		DDA->bingo = 0;
		ft_dda_steps(pool);
		ft_dda_cast(pool);
		ft_dda_calc(pool);
		ft_color_picker(pool);
		ft_foo2(x, MAP->dstart, MAP->dstop, pool);
		COLOR = 0xFFDEAD;
		ft_foo2(x, MAP->dstop + 1, WIN_H, pool);
		COLOR = 0xFFFFFF;
		ft_foo2(x, MAP->dstart, 0, pool);
	}
}
