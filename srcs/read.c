/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dmurovts <dmurovts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 10:57:50 by dmurovts          #+#    #+#             */
/*   Updated: 2017/04/01 17:16:10 by dmurovts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static	void	ft_free_char(char ***a, int deep)
{
	int i;

	i = -1;
	while (++i < deep + 1)
		free((*a)[i]);
	free(*a);
}

static int		ft_lines(int fd, int *j)
{
	int		i;
	char	*tmp;
	int		gnl;

	i = 0;
	while ((gnl = get_next_line(fd, &tmp)))
	{
		if (gnl > 0)
		{
			*j = (i == 0) ? ft_strlen(tmp) : *j;
			i++;
		}
		else if (gnl < 0)
			return (-1);
		free(tmp);
	}
	free(tmp);
	close(fd);
	return (i);
}

static char		**ft_char_read(int deep, int k, int fd)
{
	int		i;
	int		gnl;
	char	**map;

	i = -1;
	k = 0;
	map = (char **)malloc(sizeof(char *) * (deep + 1));
	while (++i < deep)
		gnl = get_next_line(fd, &map[i]);
	map[i] = NULL;
	close(fd);
	return (map);
}

static int		**ft_int_read(char **cmap, int deep, int size)
{
	int		**map;
	int		i;
	int		j;
	char	*tmp;

	i = -1;
	map = (int **)malloc(sizeof(int *) * deep);
	while (++i < deep)
	{
		j = -1;
		map[i] = (int *)malloc(sizeof(int) * size);
		while (++j < size)
		{
			tmp = ft_strsub(cmap[i], j, 1);
			map[i][j] = ft_atoi(tmp);
			free(tmp);
		}
	}
	return (map);
}

void			ft_read(int ***imap, int *d, int *s)
{
	int		deep;
	int		k;
	int		fd;
	char	**map;

	if ((fd = open("labmap", O_RDONLY)) < 0)
		return ;
	deep = ft_lines(fd, &k);
	if ((fd = open("labmap", O_RDONLY)) < 0)
		return ;
	map = ft_char_read(deep, k, fd);
	*imap = ft_int_read(map, deep, k);
	*d = deep;
	*s = k;
	ft_free_char(&map, deep);
}
